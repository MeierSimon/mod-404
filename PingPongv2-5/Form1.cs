﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PingPong2
{
    public partial class Form1 : Form
    {
        private int _directionX = 5;
        private int _directionY = 2;
        private Spielstand currentspielstand;
        public Form1()
        {
            InitializeComponent();
            currentspielstand = new Spielstand();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            tmrSpiel.Enabled = true;
            tmrSpiel.Start();
        }

        private void tmrSpiel_Tick(object sender, EventArgs e)
        {
            picBall.Location = new Point(picBall.Location.X + _directionX, picBall.Location.Y + _directionY); 
            
            if (picBall.Location.X >= pnlSpiel.Width - picBall.Width)
            {
                ballverlässtfeld();
            }

            if (picBall.Location.X <= 0)
            {
                _directionX = -_directionX;
            }

            if (picBall.Location.Y >= pnlSpiel.Height - picBall.Height)
            {
                _directionY = -_directionY;
            }

            if (picBall.Location.Y < 0)
            {
                _directionY = -_directionY;
            }
            
        }

        private void ballverlässtfeld()
        {
            currentspielstand.punktestandspieler1 += 10;
            txtPunkte.Text ="" + currentspielstand.punktestandspieler1;
            tmrSpiel.Enabled = false;
            this.picBall.Location = new System.Drawing.Point(89, 128);
        }

       
    }
}