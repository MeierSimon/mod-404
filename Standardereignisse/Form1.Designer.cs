﻿using System.Windows.Forms;

namespace StandardEreignisse
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtAusgabe = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtAusgabe
            // 
            this.txtAusgabe.Enabled = false;
            this.txtAusgabe.Location = new System.Drawing.Point(19, 18);
            this.txtAusgabe.Multiline = true;
            this.txtAusgabe.Name = "txtAusgabe";
            this.txtAusgabe.Size = new System.Drawing.Size(399, 417);
            this.txtAusgabe.TabIndex = 0;
            //this.txtAusgabe.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtAusgabe_TextChanged);
            //this.txtAusgabe.MouseClick += new System.Windows.Forms.MouseEventHandler(this.writeText);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 462);
            this.Controls.Add(this.txtAusgabe);
            this.Name = "Form1";
            this.Text = "Form1";
            this.MouseClick += new MouseEventHandler(this.txtAusgabe_TextChanged);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.writeText);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.TextBox txtAusgabe;
    }
}