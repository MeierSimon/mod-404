﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StandardEreignisse
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }

        private void txtAusgabe_TextChanged(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    txtAusgabe.Text += "Klick";
                    break;
                case MouseButtons.Right:
                    txtAusgabe.Text += "Rechtsklick";
                    break;
            }
        }

        private void writeText(object sender, MouseEventArgs e)
        {
            txtAusgabe.Text += "Hallo\r\n";
        }
    }
}