﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Label = System.Reflection.Emit.Label;

namespace Schachbrett
{
    public partial class Form1 : Form
    {
        public int Breite;
        public int Höhe;
        
        
        public Form1()
        {
            InitializeComponent();
        }
        
        private void btnStart_Click(object sender, EventArgs e)
        {
            ZeichneSchachbrett((int) nudAnzahlSeiten.Value);
        }

        private void ZeichneSchachbrett(int anzahl)
        {
            var brettbeite = pnlSchachbrett.Width;
            var bretthöhe = pnlSchachbrett.Height;
            var zellenbreite = brettbeite / anzahl;
            var zellenhöhe = bretthöhe / anzahl;
            var geradeZeile = true;
            
            pnlSchachbrett.Controls.Clear();
            for (var y = 0; y + zellenhöhe <= bretthöhe; y = y + zellenhöhe)
            {
                var startpunkt = geradeZeile ? zellenbreite : 0;
                geradeZeile = !geradeZeile;
                for (var x = startpunkt; x + zellenbreite <= brettbeite; x = x + (2 * zellenbreite))
                {
                    var zelle = new Label
                    {
                        BackColor = Color.Black,
                        Location = new Point (x, y),
                        Width = zellenbreite,
                        Height = zellenhöhe
                    };
                    pnlSchachbrett.Controls.Add(zelle);
                }
            }


        }


        
    }
}